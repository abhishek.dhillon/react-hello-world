import React from 'react';
//import logo from './logo.svg';
import './App.css';
import PostList from './React-Http/PostList';
import PostForm from './React-Http/PostForm';
// import ComponentC from './Context/ComponentC';
// import { UserProvider } from './Context/userContext';
// import ClickCounterTwo from './RenderProps/ClickCounterTwo';
// import HoverCounterTwo from './RenderProps/HoverCounterTwo';
// import User from './RenderProps/User';
// import RenderCounter from './RenderProps/RenderCounter';
// import ParentComp from './components/ParentComp';
// import RefsDemo from './components/RefsDemo';
// import PortalDemo from './components/PortalDemo';


// import { Greet } from './components/Greet';
// import Welcome from './components/Welcome';
// // import Hello from './components/Hello';
// // import Message from './components/Message';
// import Counter from './components/Counter';
// import FunctionClick from './components/FunctionClick';
// import ClassClick from './components/ClassClick';
// import EventBind from './components/EventBind';

// import ParentComponent from './components/ParentComponent';

// import UserList from './components/UserList';

// import Form from './components/Form';

// import LifeCycleA from './components/LifeCycleA';
// import FragmentDemo from './components/FragmentDemo';

// import Hero from './components/Hero';
// import ErrorBoundary from './components/ErrorBoundary';

// import ClickCounter from './HigherOrderComponents/ClickCounter';
// import HoverCounter from './HigherOrderComponents/HoverCounter';
//Stateless functional component
function App() {
  return (
    <div className="App">
      {
        <PostForm />
      }
      {
        <PostList></PostList>
      }
      {
        //Step2: Provide a Context value
        // <UserProvider value="Abhishek">
        //   <ComponentC />
        // </UserProvider>
      }
      {
        // <HoverCounter />
      }
      {
        // <ClickCounter />
      }
      {
        // <RenderCounter render={(count, incrementCount) => <ClickCounterTwo count={count} incrementCount={incrementCount} />}/> 
      }
      {
        // <RenderCounter render={(count, incrementCount) => <HoverCounterTwo count={count} incrementCount={incrementCount} />}/>
      }
      {
        // <User name={(isLoggedIn) => isLoggedIn?"abhishek": "Guest"} />
      }
      {
        //<LifeCycleA />
        // <FragmentDemo />
        // <ParentComp></ParentComp>
        // <RefsDemo></RefsDemo>
        // <PortalDemo />

      }
      {
        //       <ErrorBoundary>
        //         <Hero heroname="Captain America"></Hero>
        //       </ErrorBoundary>
        //       <ErrorBoundary>
        //         <Hero heroname="IronMan"></Hero>
        //       </ErrorBoundary>
        //       <ErrorBoundary>
        //         <Hero heroname="Joker" />
        //       </ErrorBoundary>
      }
      {
        //<Form />
      }
      {
        //<Counter></Counter>
        //       {
        //         //<Message></Message>
        //       }
        //       <Greet name = "Thanos">
        //         <p> I killed half of the population </p>
        //       </Greet>
        //       {
        //       // <Greet name = "Marvel" />
        //       <Welcome name = "Abhishek"/>
        //       // <Hello />
        //       }

        //       <FunctionClick></FunctionClick>
        //       <ClassClick></ClassClick>
        //       <EventBind></EventBind>
        //       BREAK

        //       <ParentComponent/>

        //       <UserList/>
      }
    </div>
  );
}

export default App;
