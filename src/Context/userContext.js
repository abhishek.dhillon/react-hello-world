import React from 'react';

//Creating the User Context
const UserContext = React.createContext();

//Provide this UserContext using UserProvider Component
//UserProvider Component is responsible for providing a value for all the descendant components
const UserProvider = UserContext.Provider
const UserConsumer = UserContext.Consumer

export {UserProvider, UserConsumer}