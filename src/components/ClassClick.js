import React, { Component } from 'react'

export class ClassClick extends Component {

    clickHandler() {
        console.log("Class click handler");
    }
    render() {
        return (
            <div>
                <button onClick = {this.clickHandler}>Click_Me </button>
            </div>
        )
    }
}

export default ClassClick
