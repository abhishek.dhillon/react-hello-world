import React, { Component } from 'react'

class Form extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             username: "",
             comments: ""
        }
    }
    
    handleUserNameChange = (event) => {
        this.setState({
            username: event.target.value
        })
    }

    handleCommentChange = (event) => {
        this.setState({
            comments: event.target.value
        })
    }

    handleSubmitEvent = (event) => {
        alert(`${this.state.username} commented ${this.state.comments}`)
    }
    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmitEvent}>
                    <div>
                        <label>Username</label>
                        <input type="text" value={this.state.username} onChange={this.handleUserNameChange}></input>
                    </div>
                    <div>
                        <label>Comments:</label>
                        <textarea value={this.state.comments} onChange={this.handleCommentChange}></textarea>
                    </div>
                    <div>
                        <button type="submit">Submit</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default Form
