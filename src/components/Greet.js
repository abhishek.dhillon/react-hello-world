import React from 'react';

// export default function Greet(){
//     return <h1> Hello I'm E.D.I.T.H </h1>
// }


// export const Greet = (props) => {
//     console.log(props);
//     return (
//         <div>
//             <h1> {props.name} : I'm Inevitable with JSX version of Greet Component </h1>
//             <h2> {props.children} </h2>
//         </div>
//     )
// }


//Two ways of destructuring Props object

//1st way = Destructuring props object in function body
export const Greet = (props) => {
    const {name, children} = props;
    return (
        <div>
            <h1> {name} : I'm Inevitable with JSX version of Greet Component </h1>
            <h2> {children} </h2>
        </div>
    )
}


//2nd way = Destructuring props object in parameter
// export const Greet = ({name, children}) => {
//     return (
//         <div>
//             <h1> {name} : I'm Inevitable with JSX version of Greet Component </h1>
//             <h2> {children} </h2>
//         </div>
//     )
// }

// export default Greet