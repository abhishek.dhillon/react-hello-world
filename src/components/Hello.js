import React from 'react';

const Hello = () => {
    return React.createElement(
        'div',
        { id: 'Hello', className: 'HelloComponent' },
        React.createElement(
            'h2',
            null,
            'This is html version of Hello Component')
    );
}

export default Hello