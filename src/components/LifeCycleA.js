import React, { Component } from 'react'
import LifeCycleB from './LifeCycleB';

class LifeCycleA extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            name: "Abhishek"
        }
        console.log("A Mounting LifeCycle => Constructor");
    }

    static getDerivedStateFromProps(props, state) {
        console.log("A Mounting LifeCycle => getDerivedStateFromProps");
        return null;
    }

    componentDidMount() {
        console.log("A Mounting LifeCycle => componentDidMount");
    }

    shouldComponentUpdate() {
        console.log("A Updating LifeCycle => shouldComponentUpdate");
        return true;
    }

    componentDidUpdate() {
        console.log("A Updating Lifecycle => componentDidUpdate");
    }
    
    getSnapshotBeforeUpdate(prevProps, prevState) {
        console.log("A Updating LifeCycle => getSnapshotBeforeUpdate")
        return null;
    }

    changeState = () => {
        this.setState({
            name: "CodeEvolution"
        })
    }

    render() {
        console.log("A Mounting LifeCycle => render()");
        return (
            <div>
                Mounting LifeCycle Methods A
                <LifeCycleB />              
                <button onClick={this.changeState}>Change State</button>      
            </div>
        )
    }
}

export default LifeCycleA
