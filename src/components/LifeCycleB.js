import React, { Component } from 'react'

class LifeCycleB extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            name: "Abhishek"
        }
        console.log("B Mounting LifeCycle => Constructor");
    }

    static getDerivedStateFromProps(props, state) {
        console.log("B Mounting LifeCycle => getDerivedStateFromProps");
        return null;
    }

    componentDidMount() {
        console.log("B Mounting LifeCycle => componentDidMount");
    }

    shouldComponentUpdate() {
        console.log("B Updating LifeCycle => shouldComponentUpdate");
        return true;
    }

    componentDidUpdate() {
        console.log("B Updating Lifecycle => componentDidUpdate");
    }
    
    getSnapshotBeforeUpdate(prevProps, prevState) {
        console.log("B Updating LifeCycle => getSnapshotBeforeUpdate");
        return null;
    }

    
    render() {
        console.log("B Mounting LifeCycle => render()");
        return (
            <div>
                Mounting LifeCycle Methods  B          
            </div>
        )
    }
}

export default LifeCycleB
