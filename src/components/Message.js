import React, { Component } from 'react';

export default class Message extends Component {
    constructor() {
        super()
        this.state = {
            message: "Welcome To the state property of react"
        }
    }

    changeMessage() {
        this.setState({
            message: "Thank You For Subscribing"
        })
    }

    render() {
        return (
            <div>
                <h1>{this.state.message}</h1>
                <button onClick = { () => this.changeMessage() }> Subscibe </button>
            </div>
        )
    }
}