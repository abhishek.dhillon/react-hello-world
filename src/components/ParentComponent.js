import React, { Component } from 'react'
import ChildComponent from './ChildComponent';


export class ParentComponent extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }
    
    greetParent = () => {
        alert("Hello Parents!!!!!!");
    }

    render() {
        return (
            <div>
                <ChildComponent greetHandler= {this.greetParent}></ChildComponent>
            </div>
        )
    }
}

export default ParentComponent
