import React from 'react'

function ShowUser(props) {
    console.log(props.user);
    var {name, enroll, company} = props.user
    return (
        <div>
            <h1>{name}, (Enrollment No.= {enroll}) selected in {company}</h1>
        </div>
    )
}

export default ShowUser
