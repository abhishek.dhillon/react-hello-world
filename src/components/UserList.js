import React, { Component } from 'react'
import ShowUser from './ShowUser';

class UserList extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }    
    render() {
        var users = [
            {"name": "Abhishek", "enroll": "A6561", "company": "Paxcom", "id": 1},
            {"name": "Vishal", "enroll": "A6542", "company": "Campegimini", "id": 2},
            {"name": "Ayush", "enroll": "A6567", "company": "Badi Company", "id": 3},
            {"name": "Himanshu", "enroll": "A6523", "company": "Accenture", "id": 4}
        ]
    
        var personList = users.map(user => {
            return <ShowUser key = {user.id} user={user}></ShowUser>
        });

        return (
            <div>
                {personList}
            </div>
        )
    }
}

export default UserList
