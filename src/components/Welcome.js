import React, { Component } from 'react';


// class Welcome extends React.Component {
//     render(){
//         return <h2>Basic Learning of ReactJS</h2>
//     }
// }

// class Welcome extends Component {
//     render(){
//         return <h2> {this.props.name} : Basic Learning of ReactJS</h2>
//     }
// }



//Destructuring props in class component
class Welcome extends Component {
    render(){
        const {name} = this.props;
        return <h2> {name} : Basic Learning of ReactJS</h2>
    }
}


export default Welcome;


